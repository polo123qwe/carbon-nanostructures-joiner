class Graph:
    def __init__(self, num_of_nodes):
        self.node_count = num_of_nodes
        nodes_as_range = range(self.node_count)

        self.edge_list = {node: set() for node in nodes_as_range}
        self.edge_weights = {}

    def add_edge(self, node1, node2, weight=0):
        self.edge_list[node1].add((node2, weight))
        self.edge_list[node2].add((node1, weight))
        self.edge_weights[(node1,node2)] = weight
        self.edge_weights[(node2,node1)] = weight

    def print_adj_list(self):
        for key in self.edge_list.keys():
            print(f"Node {key}: {self.edge_list[key]}")