from math import dist, lgamma
import numpy as np
from scipy.optimize import least_squares
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation as R
from Constants import C_C_BOND_LENGTH, THRESHOLD

class Joiner:
    def __init__(self, graphene, cnt, solutions, n, m) -> None:
        self.graphene = graphene
        self.cnt = cnt
        self.solutions = solutions
        self.n = n
        self.m = m
        self.num_atoms_to_combine = n+m
        self.dangling_nodes = self.find_danging_nodes(self.cnt)

    def find_danging_nodes(self, carbon_tube):
        #distances = carbon_tube.get_distances()
        nodes_below_2a = [i for i, x in enumerate(
            carbon_tube) if x.position[2] <= C_C_BOND_LENGTH]

        dangling_nodes = []
        for node_below_2a in nodes_below_2a:
            connected_to = [i for i, x in enumerate(carbon_tube) if node_below_2a != i and round(
                carbon_tube.get_distance(node_below_2a, i), 6) <= C_C_BOND_LENGTH*1.5]
            is_dangling = all(carbon_tube[node_below_2a].position[2] -
                              THRESHOLD < carbon_tube[x].position[2] for x in connected_to)
            if is_dangling:
                dangling_nodes.append(node_below_2a)

        return dangling_nodes

    def lsq(self, params, gra, gra_points, cnt, sol_avg):
        X, Y, l, theta = params
        r = R.from_euler('xyz', [0, 0, theta])

        dists = []
        for i in range(self.num_atoms_to_combine):
            gra_pos = gra[gra_points[i]].position
            gra_pos = np.array([gra_pos[0] + X, gra_pos[1] + Y, 0])

            cnt_pos = cnt[self.dangling_nodes[i]].position

            cnt_pos = r.apply(cnt_pos)
            cnt_pos = np.array([cnt_pos[0] + sol_avg[0], cnt_pos[1] + sol_avg[1], cnt_pos[2]])

            dists.append(np.power(np.linalg.norm(gra_pos - cnt_pos) - C_C_BOND_LENGTH, 2))

        return sum(dists)

    def get_solution_center(self, graphene, solution):
        x_points = [graphene[x].position[0] for x in solution]
        y_points = [graphene[x].position[1] for x in solution]

        avg_x = np.average(x_points)
        avg_y = np.average(y_points)
        return (avg_x, avg_y)


    def join(self, solution):
        gra = self.graphene.copy()
        gra_points = solution
        
        sol_avg = self.get_solution_center(gra, solution)

        #cnt, cnt_points = self.reposition_cnt(gra, sol_avg)
        cnt = self.cnt.copy()

        params0 = [0, 0, 0, 0]
        res = least_squares(self.lsq, params0, args=(
            [gra, gra_points, cnt, sol_avg]))
        print(f"{res.cost=} {res.x=}")
        return res

    def reposition_cnt(self, cnt, distance, r=None):
        cnt = cnt.copy()
        cnt_points = self.dangling_nodes

        for atom in cnt:
            if r != None:
                atom.position = r.apply(atom.position)
            atom.position = [atom.position[0] + distance[0],
                             atom.position[1] + distance[1], atom.position[2]]

        return cnt, cnt_points

    def reposition_graphene(self, graphene, distance):
        graphene = graphene.copy()

        for atom in graphene:
            atom.position = [atom.position[0] + distance[0],
                             atom.position[1] + distance[1], atom.position[2]]        
        return graphene

    def check_pairs_distance(self, gra, cnt, gra_points, cnt_points):
        for pair_idx in range(len(gra_points)):
            gra_point = gra_points[pair_idx]
            cnt_point = cnt_points[pair_idx]

            # Check graphene points close to the nanotube point
            for i, atom in gra:
                if i == gra_point:
                    continue

                if np.abs(atom.position - cnt[cnt_point].position) < C_C_BOND_LENGTH + THRESHOLD:
                    return False

            # Check nanotube points close to the graphene point
            for i, atom in cnt:
                if i == cnt_point:
                    continue

                if np.abs(atom.position - gra[gra_point].position) < C_C_BOND_LENGTH + THRESHOLD:
                    return False

    def filter_results(self, results, solutions):
        valid_results = []

        for i, res in enumerate(results):
            X, Y, l, theta = res.x
            r = R.from_euler('xyz', [0, 0, theta])

            gra = self.reposition_graphene(self.graphene, (X, Y))
            cnt, cnt_points = self.reposition_cnt(self.cnt, self.get_solution_centergra, (solutions[i]))
            for atom in cnt:
                atom.position = r.apply(atom.position)

            # Before I can do this, remove the nodes inside the selected polygon to not get false matches
            #if self.check_pairs_distance(gra, cnt, solutions[i], cnt_points):
            #    valid_results.append((res, solutions[i], i))

        return valid_results

    def find_best_junction(self, solutions):
        results = []
        for idx in range(len(solutions)):
            print(f"Processing {idx}")
            res = self.join(solutions[idx])
            results.append((res, idx))

        #results = self.filter_results(results, solutions)
        self.results = sorted(results, key=lambda x: x[0].cost)

        for i in range(3):
            print(f"[{i}]. {results[i][0].cost} {results[i][0].x} with \n {solutions[results[i][1]]}. {results[i][1]}")
        return results[0]