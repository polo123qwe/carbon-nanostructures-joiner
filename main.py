from audioop import avg
from importlib.resources import path
from ase import Atoms
from ase.build import nanotube
from ase.visualize import view
from ase.lattice.hexagonal import Graphene
from scipy import rand
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import least_squares
from scipy.spatial.transform import Rotation as R
import matplotlib.path as mpltPath


from Graph import Graph
from Pathfinder import Pathfinder
from Joiner import Joiner
from Constants import NG_TYPE_0, NG_TYPE_1, NG_TYPE_2, NG_TYPE_3, C_C_BOND_LENGTH


def generate_graph(graphene):
    graph = Graph(len(graphene))
    distances = graphene.get_all_distances()

    for i, atom_distances in enumerate(distances):
        dists = [round(b, 6) for j, b in enumerate(atom_distances) if b != 0]
        dist_neighbour_1 = min(dists)

        dist_neighbour_2 = min(filter(lambda x: x != dist_neighbour_1, dists))

        dist_neighbour_3 = min(
            filter(lambda x: x != dist_neighbour_1 and x != dist_neighbour_2, dists))

        # print(f"{i}: {dist_neighbour_3}")

        for j, dist in enumerate(atom_distances):
            dist = round(dist, 6)
            if i == j:
                continue

            neighbour_type = NG_TYPE_0
            if dist == dist_neighbour_1:
                neighbour_type = NG_TYPE_1
                graph.add_edge(i, j, neighbour_type)
            elif dist == dist_neighbour_2:
                neighbour_type = NG_TYPE_2
                graph.add_edge(i, j, neighbour_type)
            elif dist == dist_neighbour_3:
                neighbour_type = NG_TYPE_3
                graph.add_edge(i, j, neighbour_type)

    # graph.print_adj_list()
    return graph

def remove_points(graphene, solution):
    points = [[graphene[x].position[0], graphene[x].position[1]]
              for x in solution]

    path = mpltPath.Path(points)

    atoms_to_remove = []
    for i, atom in enumerate(graphene):
        pos = [atom.position[0], atom.position[1]]
        if pos in points:
            continue

        if path.contains_point(pos):
            atoms_to_remove.append(i)

    del graphene[atoms_to_remove]
    print(f"Removed {atoms_to_remove=} from graphene, {solution}")



def print_solution(solution, joiner, gra, cnt, lsq_res):
    X, Y, l, theta = lsq_res
    r = R.from_euler('xyz', [0, 0, theta])

    fig, ax = plt.subplots()

    ax.annotate(f"[{round(X, 3)}, {round(Y, 3)}] T:{round(theta, 3)}", (-5, 10), color="black", xytext=(
            5, 5), textcoords="offset pixels")

    # Cutout
    x_points = [x.position[0] + X for x in gra]
    y_points = [x.position[1] + Y for x in gra]

    ax.scatter(x_points, y_points, color="blue", alpha=0.7)

    gra_pos = [gra[x].position for x in solution]
    gra_pos = np.array([[x[0] + X, x[1] + Y, 0] for x in gra_pos])
    x_points, y_points, _ = gra_pos.T

    ax.scatter(x_points, y_points, color="blue", alpha=0.7)
    ax.plot(x_points, y_points)

    avg_x = np.average(x_points)
    avg_y = np.average(y_points)

    ax.scatter(avg_x, avg_y, color="red", alpha=0.7)

    for i, txt in enumerate(solution):
        ax.annotate(f"{txt} ({i})", (x_points[i], y_points[i]), xytext=(
            5, 5), textcoords="offset pixels")


    # Original CNT
    cnt_pos = [cnt[x].position for x in joiner.dangling_nodes]
    cnt_pos = np.array([[x[0] + avg_x, x[1] + avg_y, x[2]] for x in cnt_pos])
    x_points, y_points, _ = cnt_pos.T

    ax.scatter(x_points, y_points, color="green", alpha=0.7)
    for i, txt in enumerate(joiner.dangling_nodes):
        ax.annotate(f"{txt} ({i})", (x_points[i], y_points[i]), color="grey", xytext=(
            5, 5), textcoords="offset pixels")

    ###

    cnt_pos = [r.apply(cnt[x].position) for x in joiner.dangling_nodes]
    cnt_pos = np.array([[x[0] + avg_x, x[1] + avg_y, x[2]] for x in cnt_pos])
    x_points, y_points, _ = cnt_pos.T

    ax.scatter(x_points, y_points, color="red", alpha=0.7)
    for i, txt in enumerate(joiner.dangling_nodes):
        ax.annotate(f"{txt} ({i})", (x_points[i], y_points[i]), color="red", xytext=(
            5, 5), textcoords="offset pixels")

    # plt.show()


def main():
    n = 4
    m = 4
    tube_length = 6

    carbon_tube = nanotube(n, m, length=tube_length, symbol='C')

    num_atoms_to_combine = n + m

    index1 = 6
    index2 = 6
    alat = 2.45
    clat = 31.85
    graphene = Graphene(symbol='C', latticeconstant={
                        'a': alat, 'c': clat}, size=(index1, index2, 1))
    graph = generate_graph(graphene)
    pathfinder = Pathfinder(graphene, graph, num_atoms_to_combine)
    solutions = pathfinder.generate_paths()
    #solutions = [[37, 25, 26, 40, 52, 51, 49, 48, 37], [37, 38, 41, 66, 65, 63, 49, 48, 37], [37, 25, 27, 52, 64, 63, 49, 48, 37], [37, 38, 52, 64, 63, 61, 60, 48, 37], [37, 38, 41, 66, 65, 63, 60, 48, 37], [37, 39, 52, 66, 65, 63, 60, 48, 37], [37, 38, 52, 53, 65, 63, 60, 48, 37], [37, 25, 38, 52, 64, 63, 60, 48, 37], [37, 36, 38, 52, 64, 63, 60, 48, 37], [37, 25, 27, 52, 64, 63, 60, 48, 37], [37, 38, 40, 43, 55, 66, 64, 51, 37], [37, 25, 26, 29, 54, 66, 64, 51, 37], [37, 25, 27, 40, 54, 66, 64, 51, 37], [37, 36, 24, 27, 41, 66, 64, 51, 37], [37, 36, 24, 13, 15, 40, 52, 51, 37], [37, 25, 26, 28, 42, 54, 53, 51, 37], [37, 36, 24, 26, 29, 54, 53, 51, 37], [37, 36, 24, 26, 40, 41, 53, 51, 37], [37, 25, 26, 29, 54, 66, 65, 62, 37], [37, 25, 27, 40, 54, 66, 65, 62, 37], [37, 36, 25, 27, 41, 66, 65, 62, 37], [37, 36, 24, 27, 41, 66, 65, 62, 37], [37, 36, 38, 41, 55, 67, 65, 62, 37], [37, 38, 40, 54, 55, 67, 65, 62, 37], [37, 38, 40, 43, 68, 67, 65, 62, 37], [37, 36, 24, 27, 41, 53, 64, 62, 37], [37, 25, 26, 40, 41, 53, 64, 62, 37], [37, 36, 24, 13, 27, 52, 64, 62, 37], [37, 38, 40, 54, 66, 65, 63, 49, 37], [37, 25, 27, 41, 66, 65, 63, 49, 37], [37, 36, 38, 41, 66, 65, 63, 49, 37], [37, 25, 38, 52, 53, 65, 63, 49, 37], [37, 36, 38, 52, 53, 65, 63, 49, 37], [37, 36, 25, 27, 52, 64, 63, 49, 37], [37, 36, 24, 27, 52, 64, 63, 49, 37], [37, 25, 27, 41, 53, 64, 62, 49, 37], [37, 36, 24, 27, 52, 64, 62, 49, 37]]
    joiner = Joiner(graphene, carbon_tube, solutions, n, m)
    best_reward, best_idx = joiner.find_best_junction(solutions)
    #best_reward, best_idx = [{"x": [-0.10650196, -0.25187419, 0., -2.53528728]}, 2]
    '''
    for i in range(5):
        best_reward, best_idx = joiner.results[i]
        print_solution(solutions[best_idx], joiner,
                       graphene, carbon_tube, best_reward.x)
    '''
    print_solution(solutions[best_idx], joiner, graphene, carbon_tube, best_reward.x)
    #view(carbon_tube)
    #remove_points(graphene, solutions[best_idx])
    #view(graphene)

    #print_solution(solutions[55], joiner, graphene, carbon_tube, [0.08554287, -0.09471256, 0., 1.2240114])

    plt.show()


if __name__ == '__main__':
  main()