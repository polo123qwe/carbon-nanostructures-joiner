from ast import pattern
import math
import numpy as np
from scipy.fft import next_fast_len
from Constants import NG_TYPE_0, NG_TYPE_1, NG_TYPE_2, NG_TYPE_3, C_C_BOND_LENGTH


class Pathfinder:
    def __init__(self, atoms, graph, max_iter) -> None:
        self.atoms = atoms
        self.graph = graph
        self.max_iter = max_iter

    def get_angle_2d(self, a1, a2, a3):
        a = np.array(self.atoms[a1].position)
        b = np.array(self.atoms[a2].position)
        c = np.array(self.atoms[a3].position)

        angle = math.degrees(math.atan2(
            c[1]-b[1], c[0]-b[0]) - math.atan2(a[1]-b[1], a[0]-b[0]))
        angle = angle + 360 if angle < 0 else angle
        return round(angle, 0)

    def get_leftmost_neighbour(self, node, neighbour, neighbour_type, visited=[]):
        angle = 30
        if neighbour_type == NG_TYPE_2:
            angle = 60

        neighbours = [x[0]
                      for x in self.graph.edge_list[node] if x[1] == neighbour_type]
        neighbours_filtered = filter(lambda x: round(
            self.atoms.get_angle(node, neighbour, x), 0) == angle, neighbours)
        for new_neighbour in neighbours_filtered:
            if self.get_angle_2d(neighbour, node, new_neighbour) == 30:
                return new_neighbour
        return -1

    def is_node_direct_neighbour(self, node, neighbour, neighbour_type, node_to_check):
        dist_neighbour = round(
            self.atoms.get_distance(neighbour, node_to_check), 6)
        dist_node = round(self.atoms.get_distance(node, node_to_check), 6)
        if neighbour_type == NG_TYPE_2 and dist_neighbour == C_C_BOND_LENGTH and dist_node == C_C_BOND_LENGTH:
            return True
        if neighbour_type == NG_TYPE_3 and dist_node == C_C_BOND_LENGTH:
            return True
        return False

    def get_connected_neighbours(self, node, neighbour):
        neighbours_edge = [
            x for x in self.graph.edge_list[node] if x[0] == neighbour][0]

        neighbours = []
        if neighbours_edge[1] == NG_TYPE_3:
            neighbours_2 = [x[0]
                            for x in self.graph.edge_list[node] if x[1] == NG_TYPE_2]

            neighbours_filtered = filter(lambda x:
                                         round(
                                             self.atoms.get_angle(node, neighbour, x), 0) == 60, neighbours_2)
            neighbours.extend(neighbours_filtered)

        if neighbours_edge[1] == NG_TYPE_3 or neighbours_edge[1] == NG_TYPE_2:
            neighbours_1 = [x[0]
                            for x in self.graph.edge_list[node] if x[1] == NG_TYPE_1]
            neighbours_filtered = filter(lambda x: round(
                self.atoms.get_angle(node, neighbour, x), 0) == 30, neighbours_1)
            # neighbours_filtered = filter(lambda x: self.is_node_direct_neighbour(node, neighbour, x, neighbours_edge[1]), neighbours_filtered)
            neighbours.extend(neighbours_filtered)

        return neighbours

    def check_neighbours(self, node, neighbour3, visited=[]):
        selected_neighbour3 = None
        selected_neighbour2 = None
        selected_neighbour1 = None

        if neighbour3 in visited:
            return

        neighbour2 = self.get_leftmost_neighbour(
            node, neighbour3, NG_TYPE_2, visited)
        if (neighbour2 != -1 and neighbour2 not in visited):
            # print(f"Found valid neighbour 2: {neighbour2}")
            selected_neighbour3 = neighbour3
            selected_neighbour2 = neighbour2

        if selected_neighbour2 == None:
            return

        neighbour1 = self.get_leftmost_neighbour(
            node, selected_neighbour2, NG_TYPE_1, visited)
        if (neighbour1 != -1 and neighbour1 not in visited):
            # print(f"Found valid neighbour 1: {neighbour1}")
            selected_neighbour1 = neighbour1

        if selected_neighbour1 == None:
            return

        return selected_neighbour1, selected_neighbour2, selected_neighbour3

    def get_neighbours(self, node, previous, visited=[]):
        neighbours = [x[0]
                      for x in self.graph.edge_list[node] if x[0] not in visited]
        if previous is None:
            return neighbours
        neighbours = list(filter(lambda x: self.get_angle_2d(
            previous, node, x) <= 150, neighbours))
        return neighbours

    # Pick random point
    # Pick a neighbour 1
    #   Find neighbour 2 directly connected to the neighbour 1
    #   Find neighbour 3 directly connected to the neighbour 2
    # Repeat algorithm from those 3 points

    def filter_solutions(self):
        new_solutions = []
        patterns_found = []

        for solution in self.solutions:
            current_pattern = []

            # Last selected atom cannot exceed Y coord of starting point atom
            if self.atoms[solution[0]].position[1] < self.atoms[solution[-2]].position[1]:
                continue

            for i, step in enumerate(solution):
                if i == 0:
                    continue
                prev_step = solution[i - 1]

                neighbour = [
                    x for x in self.graph.edge_list[prev_step] if x[0] == step][0]
                current_pattern.append(neighbour[1])
                prev_step = step

            if current_pattern not in patterns_found:
                patterns_found.append(current_pattern)
                new_solutions.append(solution)

        self.solutions = new_solutions
        return patterns_found

    def exceeds_two_type_1_neighbours(self, steps):
        prev_step = None
        current_pattern = []

        for step in steps:
            if prev_step == None:
                prev_step = step
                continue
            neighbour = [
                x for x in self.graph.edge_list[prev_step] if x[0] == step][0]
            current_pattern.append(neighbour[1])
            prev_step = step
        if current_pattern.count(1) > 2:
            return True
        return False

    def invalid_angle(self, previous_node, current_node, neighbour):
        min_angle = 90
        max_angle = 270

        if previous_node == None: 
            return False

        angle = self.get_angle_2d(previous_node, current_node, neighbour)
        if angle < min_angle or angle > max_angle:
            return True
        return False

    def generate_paths(self):
        start_atom = 37  # random.randint(0, graph.node_count)
        current_atom = start_atom
        self.solutions = []
        self.find_path_recursive(current_atom, None, 0, [
                                 current_atom], [])
        print(f"Before {len(self.solutions)=}")
        self.filter_solutions()

        # Reverse the order of the solutions
        self.solutions = [x[::-1] for x in self.solutions]

        print(f"{self.solutions=}")
        print(f"After {len(self.solutions)=}")


        return self.solutions

    def find_path_recursive(self, current_node, previous_node, current_iter, steps=[], visited=[]):
        if current_iter >= self.max_iter:
            if current_node == steps[0]:
                # print("Done!", steps, "solutions,", len(self.solutions))
                self.solutions.append(steps)
                return
            return

        neighbours = self.get_neighbours(
            current_node, previous_node, visited)

        # visited = visited + [neighbour1, neighbour2, neighbour3]
        for neighbour in neighbours:
            if self.exceeds_two_type_1_neighbours(steps + [neighbour]):
                continue

            if self.invalid_angle(previous_node, current_node, neighbour):
                continue

            new_visited = visited + \
                self.get_connected_neighbours(
                    current_node, neighbour) + [neighbour]
            # print(f"{current_node=}, {neighbour=} {self.get_connected_neighbours(current_node, neighbour)} {[self.atoms.get_distance(neighbour, x) for x in self.get_connected_neighbours(current_node, neighbour)]}")
            self.find_path_recursive(
                neighbour, current_node, current_iter + 1, steps + [neighbour], new_visited)
